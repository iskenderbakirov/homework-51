import React, {Component} from 'react';
import Myheader from './components/Myheader';
import Sidebar from './components/Sidebar';
import Content from './components/Content';
import Footer from './components/Footer';

class App extends Component {
    render() {
        return (
            <React.Fragment>
                <Myheader></Myheader>
                <div className="content">
                    <div className="container">
                        <Sidebar></Sidebar>
                        <Content></Content>
                    </div>
                </div>
                <Footer></Footer>
            </React.Fragment>
        );
    }
}

export default App;
