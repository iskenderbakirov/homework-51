import React, { Component } from 'react';

class Sidebar extends Component {
    render() {
        return (
            <aside>
                <h2>News</h2>
                <Newsitem></Newsitem>
                <Newsitem></Newsitem>
                <Newsitem></Newsitem>
                <Newsitem></Newsitem>
            </aside>
        );
    }
}

class Newsitem extends Component {
    render() {
        return (
            <div className="block">
                <h3>News title!</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab, asperiores blanditiis cum dolor laborum nesciunt quisquam recusandae, reiciendis sapiente, vero voluptatem?</p>
            </div>
        );
    }
}


export default Sidebar;
