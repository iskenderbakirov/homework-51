import React, { Component } from 'react';

class Content extends Component {
    render() {
        return (
            <section>
                <h1>Lorem ipsum dolor sit amet, consectetur.</h1>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab alias commodi laudantium magnam minima perferendis quae sapiente temporibus ullam. Architecto cumque dolor est incidunt minus nemo nisi numquam optio, quisquam quo quod repellendus repudiandae veniam voluptatem?</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab alias commodi laudantium magnam minima perferendis quae sapiente temporibus ullam. Architecto cumque dolor est incidunt minus nemo nisi numquam optio, quisquam quo quod repellendus repudiandae veniam voluptatem?</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci deleniti dignissimos expedita fugit harum nostrum obcaecati. Asperiores cum dolorum eius eum exercitationem iure quae sapiente veniam voluptatibus! Ab accusantium aliquam atque aut autem blanditiis, cumque dolores eligendi enim harum hic illum maxime molestias nulla perspiciatis similique ut, veritatis. A ab accusamus animi aperiam aspernatur consectetur cum deleniti distinctio dolore, doloribus enim explicabo iure laborum magnam numquam odit optio perferendis possimus qui quia, quibusdam quo quod quos soluta tempora tempore tenetur ut voluptas voluptatibus voluptatum! Consectetur consequuntur earum in ipsa modi, nulla reprehenderit repudiandae! Accusantium, ad architecto corporis cum est hic impedit inventore modi praesentium quae quaerat reprehenderit sapiente sed tenetur, vero voluptate voluptates. Aliquam atque autem cupiditate dicta earum error facere, nobis quae.</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab amet ea eaque est id, illum in ipsam, magni natus non quia quidem, repellat voluptates? Dolores, perspiciatis.</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab amet ea eaque est id, illum in ipsam, magni natus non quia quidem, repellat voluptates? Dolores, perspiciatis.</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci deleniti dignissimos expedita fugit harum nostrum obcaecati. Asperiores cum dolorum eius eum exercitationem iure quae sapiente veniam voluptatibus! Ab accusantium aliquam atque aut autem blanditiis, cumque dolores eligendi enim harum hic illum maxime molestias nulla perspiciatis similique ut, veritatis. A ab accusamus animi aperiam aspernatur consectetur cum deleniti distinctio dolore, doloribus enim explicabo iure laborum magnam numquam odit optio perferendis possimus qui quia, quibusdam quo quod quos soluta tempora tempore tenetur ut voluptas voluptatibus voluptatum! Consectetur consequuntur earum in ipsa modi, nulla reprehenderit repudiandae! Accusantium, ad architecto corporis cum est hic impedit inventore modi praesentium quae quaerat reprehenderit sapiente sed tenetur, vero voluptate voluptates. Aliquam atque autem cupiditate dicta earum error facere, nobis quae.</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab amet ea eaque est id, illum in ipsam, magni natus non quia quidem, repellat voluptates? Dolores, perspiciatis.</p>
            </section>
        );
    }
}

export default Content;
