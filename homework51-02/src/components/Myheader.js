import React, { Component } from 'react';

class Myheader extends Component {
    render() {
        return (
            <header>
                <div className="container">
                    <a href="" className="logo">Logo</a>
                    <ul className="nav">
                        <li><a href="">Home</a></li>
                        <li><a href="">About</a></li>
                        <li><a href="">News</a></li>
                        <li><a href="">Portfolio</a></li>
                        <li><a href="">Contacts</a></li>
                    </ul>
                </div>
            </header>
        );
    }
}

export default Myheader;
