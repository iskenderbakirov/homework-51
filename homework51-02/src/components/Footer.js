import React, { Component } from 'react';

class Footer extends Component {
    render() {
        return (
            <footer>
                <div className="container">
                    <a href="" className="footer-logo">Logo</a>
                    <ul className="footer-nav">
                        <li><a href="">Home</a></li>
                        <li><a href="">About</a></li>
                        <li><a href="">News</a></li>
                        <li><a href="">Portfolio</a></li>
                        <li><a href="">Contacts</a></li>
                    </ul>
                </div>
            </footer>
        );
    }
}

export default Footer;
