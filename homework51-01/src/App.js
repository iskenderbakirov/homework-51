import React, { Component } from 'react';
import ReactDOM from "react-dom";


class App extends Component {
  render() {
    return (
      <MyFavouriteFilms/>
    );
  }
}

class MyFavouriteFilms extends Component {
    render () {
        return (
            <div className="container">
                <Film
                    title="The Avengers"
                    description="Earth's mightiest heroes must come together and learn to fight as a team if they are going to stop
                    the mischievous Loki and his alien army from enslaving humanity."
                    url="https://m.media-amazon.com/images/M/MV5BNDYxNjQyMjAtNTdiOS00NGYwLWFmNTAtNThmYjU5ZGI2YTI1XkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_SY1000_CR0,0,675,1000_AL_.jpg"
                />
                <Film
                    title="Il buono, il brutto, il cattivo "
                    description="A bounty hunting scam joins two men in an uneasy alliance against a third in a race to find a fortune in gold buried in a remote cemetery."
                    url="https://m.media-amazon.com/images/M/MV5BOTQ5NDI3MTI4MF5BMl5BanBnXkFtZTgwNDQ4ODE5MDE@._V1_SY1000_CR0,0,656,1000_AL_.jpg"
                />
                <Film
                    title="The Matrix"
                    description="A computer hacker learns from mysterious rebels about the true nature of his reality and his role in the war against its controllers."
                    url="https://m.media-amazon.com/images/M/MV5BNzQzOTk3OTAtNDQ0Zi00ZTVkLWI0MTEtMDllZjNkYzNjNTc4L2ltYWdlXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_SY1000_CR0,0,665,1000_AL_.jpg"
                />
            </div>
        )
    }
}

const Film = props => {
    return (
        <div className="film">
            <h3>{props.title}</h3>
            <p><span>About film:</span> {props.description}</p>
            <img src={props.url} alt=""/>
        </div>
    )
};

export default App;
